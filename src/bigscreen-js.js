/* eslint-disable */
class BigScreen {

    // 默认配置
    defaultProps = ({
        baseWidth: 1920,
        baseHeight: 1080,
        scaleCoeff: 0,
        useZoom: false, // 使用zoom模式缩放
    });

    constructor(props) {
        const { el, baseWidth, baseHeight, scaleCoeff, useZoom } = { ...this.defaultProps, ...props };
        this.el = el;
        this.scaleCoeff = scaleCoeff;
        this.baseWidth = baseWidth;
        this.baseHeight = baseHeight;
        this.useZoom = useZoom;

        this.init();
    }

    init() {
        this.initDomHandler();
        // 绘制
        this.calcRate();
        // 改变窗口大小重新绘制
        window.addEventListener("resize", this.resize.bind(this));
    }

    initDomHandler() {
        let db = document.querySelector(this.el);
        db.style.width = `${this.baseWidth}px`;
        db.style.height = `${this.baseHeight}px`;

        // 初始化el样式
        const elDefaultStyle = (this.useZoom ? {
            transition: 'all 2s',
            transform: 'scale(1)',
        } : {
            position: 'absolute',
            transformOrigin: 'left top 0px',
            left: '50%',
            top: '50%',
            transition: 'all 2s',
            overflow: 'auto'
        })

        for (let key in elDefaultStyle) {
            db.style[key] = elDefaultStyle[key];
        }
        this.db = db;
    }

    calcRate() {
        let baseRate = (this.baseWidth / this.baseHeight).toFixed(5);
        let currentRate = (window.innerWidth / window.innerHeight).toFixed(5);
        let db = this.db;
        if (db) {
            if (currentRate > baseRate) {
                // 以高为准
                this.scaleCoeff = (window.innerHeight / this.baseHeight).toFixed(5);
            } else {
                // 以宽为准
                this.scaleCoeff = (window.innerWidth / this.baseWidth).toFixed(5);
            }
            if (this.useZoom) {
                db.style.zoom = this.scaleCoeff;
                db.style.transform = 'scale(0.96)';
                clearTimeout(this.useZoomTimer);
                this.useZoomTimer = setTimeout(() => {
                    db.style.transform = 'scale(1)';
                }, 2000)
            } else {
                db.style.transform =
                    "scale(" + this.scaleCoeff + ") translate(-50%,-50%)";
            }
        }
    }

    resize() {
        clearTimeout(this.timer);
        this.timer = setTimeout(() => {
            this.calcRate()
        }, 100);
    }
}

export default BigScreen;

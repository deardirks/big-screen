# bigscreen-js

#### Description

Large screen adaptation scheme

#### Install

```
npm install bigscreen-js
```

#### Usage

- import

```
import BigScreen from "bigscreen-js";
const bs = new BigScreen({
    el: "#app",
    baseWidth: 1920,
    baseHeight: 1080,
});
```

- require

```
let BigScreen = require("bigscreen-js").default;
const bs = new BigScreen({
    el: "#app",
    baseWidth: 1920,
    baseHeight: 1080
});
```

#### options

```
{
    el: "#app", // dom节点
    baseWidth: 1920, // 设计稿宽度
    baseHeight: 1080, // 设计稿高度
    useZoom: false, // 使用zoom模式缩放
}
```

"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/* eslint-disable */
var BigScreen = /*#__PURE__*/function () {
  // 默认配置
  function BigScreen(props) {
    _classCallCheck(this, BigScreen);

    _defineProperty(this, "defaultProps", {
      baseWidth: 1920,
      baseHeight: 1080,
      scaleCoeff: 0,
      useZoom: false // 使用zoom模式缩放

    });

    var _this$defaultProps$pr = _objectSpread(_objectSpread({}, this.defaultProps), props),
        el = _this$defaultProps$pr.el,
        baseWidth = _this$defaultProps$pr.baseWidth,
        baseHeight = _this$defaultProps$pr.baseHeight,
        scaleCoeff = _this$defaultProps$pr.scaleCoeff,
        useZoom = _this$defaultProps$pr.useZoom;

    this.el = el;
    this.scaleCoeff = scaleCoeff;
    this.baseWidth = baseWidth;
    this.baseHeight = baseHeight;
    this.useZoom = useZoom;
    this.init();
  }

  _createClass(BigScreen, [{
    key: "init",
    value: function init() {
      this.initDomHandler(); // 绘制

      this.calcRate(); // 改变窗口大小重新绘制

      window.addEventListener("resize", this.resize.bind(this));
    }
  }, {
    key: "initDomHandler",
    value: function initDomHandler() {
      var db = document.querySelector(this.el);
      db.style.width = "".concat(this.baseWidth, "px");
      db.style.height = "".concat(this.baseHeight, "px"); // 初始化el样式

      var elDefaultStyle = this.useZoom ? {
        transition: 'all 2s',
        transform: 'scale(1)'
      } : {
        position: 'absolute',
        transformOrigin: 'left top 0px',
        left: '50%',
        top: '50%',
        transition: 'all 2s',
        overflow: 'auto'
      };

      for (var key in elDefaultStyle) {
        db.style[key] = elDefaultStyle[key];
      }

      this.db = db;
    }
  }, {
    key: "calcRate",
    value: function calcRate() {
      var baseRate = (this.baseWidth / this.baseHeight).toFixed(5);
      var currentRate = (window.innerWidth / window.innerHeight).toFixed(5);
      var db = this.db;

      if (db) {
        if (currentRate > baseRate) {
          // 以高为准
          this.scaleCoeff = (window.innerHeight / this.baseHeight).toFixed(5);
        } else {
          // 以宽为准
          this.scaleCoeff = (window.innerWidth / this.baseWidth).toFixed(5);
        }

        if (this.useZoom) {
          db.style.zoom = this.scaleCoeff;
          db.style.transform = 'scale(0.96)';
          clearTimeout(this.useZoomTimer);
          this.useZoomTimer = setTimeout(function () {
            db.style.transform = 'scale(1)';
          }, 2000);
        } else {
          db.style.transform = "scale(" + this.scaleCoeff + ") translate(-50%,-50%)";
        }
      }
    }
  }, {
    key: "resize",
    value: function resize() {
      var _this = this;

      clearTimeout(this.timer);
      this.timer = setTimeout(function () {
        _this.calcRate();
      }, 100);
    }
  }]);

  return BigScreen;
}();

var _default = BigScreen;
exports["default"] = _default;